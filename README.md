# setup-ssl-with-certbot

Obtain ssl certificate from Let's Encrypt using certbot.

# Usage:

**setup-ssl.sh** 'your_domain_name' 'apache_config_file'

Scripts works under Centos and Ubuntu servers.

First stops apache and nginx than obtains certificates by certbot from Let's Encrypt and at the end configures apache's config file.
