#!/bin/bash

function whichos() {
    # Usage:
    #   whichos
    # Returns name of OS

    local OS_NAME=''
    if [ -f /etc/lsb-release ]
    then
        OS_NAME="Ubuntu"
    else
        OS_NAME="Centos"
    fi

    echo "$OS_NAME"
}

function certbot_ubuntu() {
    # Usage:
    #   certbot_ubuntu
    # Install certbot for ubuntu platform

    apt-get update -y
    apt-get install -y software-properties-common
    add-apt-repository universe
    add-apt-repository -y ppa:certbot/certbot
    apt-get update -y
    apt-get install -y certbot

    return $?
}

function certbot_centos() {
    # Usage:
    #   certbot_centos
    # Install certbot for centos platform

    yum install -y epel-release
    yum install -y certbot

    return $?
}

function certbot_get_cert() {
   # Usage:
   #   certbot_get_cert 'domain-name'
   # Function obtains certificate using certbot

   certbot certonly --standalone -d "$1"
   return $?
}

function certbot_configure_file() {
    # Usage:
    #   certbot_configure_file 'config-file-name'
    # Setup given apache config file

    config_file="$1"
    cp "$config_file" "$config_file_$(date '+%s')"

    certPath=$(certbot certificates --cert-name $domain  2>/dev/null | grep 'Certificate Path' | cut -d ':' -f 2)
    privKeyPath=$(certbot certificates --cert-name $domain  2>/dev/null | grep 'Private Key Path' | cut -d ':' -f 2)
    sed -i -e "s:\(SSLCertificateFile \).*:\1 $certPath:" "$config_file"
    sed -i -e "s:\(SSLCertificateKeyFile \).*:\1 $privKeyPath:" "$config_file"

    return $?
}

domain="$1"
if [[ -z "$domain" || "$domain" =~ [0-9]+\.[0-9]+\.[0-9]+\.[0-9]+ ]]; then
    echo "Error: Domain name is not provided!"
    exit 1
fi

apache_config="$2"
if [[ -z "$apache_config" ]]; then
    echo "Error: Config file is not given!"
    exit 1
fi

os=$(whichos)
apache='httpd'
if [ "$os" = 'Ubuntu' ]; then
    apache='apache2' 
fi 

# Stop apache and nginx if running
echo 'Stopping web services...'
systemctl stop "$apache"

stop_nginx=0
if systemctl status nginx >/dev/null ; then
    stop_nginx=1
    systemctl stop nginx
fi

echo "Installing certbot for $os..."
if [ "$os" = 'Ubuntu' ]; then
    certbot_ubuntu >/dev/null
else
    certbot_centos >/dev/null
fi

if [ $? -eq 0 ]; then
    echo "Get ssl certificate..."
    certbot_get_cert "$domain"
else
    echo 'Error: Could not install certbot'
    exit 1
fi

if [ $? -eq 0 ]; then
    echo "Configuration of apache file: $apache_config"
    certbot_configure_file "$apache_config"
else
    echo 'Error: Could not obtain ssl certificate'
    exit 1
fi

# Start apache and nginx if needed
echo 'Starting web services...'
systemctl start "$apache"
if [ $stop_nginx -eq 1 ]; then
    systemctl start nginx
fi

exit 0

